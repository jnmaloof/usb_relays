import sys
import serial
import time

portName = "/dev/tty.usbmodem621"
serPort = serial.Serial(portName, 19200, timeout = 1)

if time.gmtime(time.time())[4] < 20 or time.gmtime(time.time())[4] > 40:
    num = 1
    while num < 13:
        if num < 10:
            relayIndex = str(num)
            num += 1
            serPort.write("relay on " + relayIndex +"\n\r")
            time.sleep(60)
            serPort.write("relay off " + relayIndex +"\n\r")
        else:
            relayIndex = chr(55 + int(num))
            num += 1
            serPort.write("relay on " + relayIndex +"\n\r")
            time.sleep(60)
            serPort.write("relay off " + relayIndex +"\n\r")

relayn = int(sys.argv[1])
portname = int(sys.argv[2])

#Add port as a command line argument


